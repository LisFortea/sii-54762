
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include "DatosMemCompartida.h"


int main(int argc, char **argv) {
int fd;
DatosMemCompartida *datosb;

 
     if ((fd=open("mmap_bot", O_RDWR))<0) {
        perror("No puede abrirse el fichero para mmap");
        return 1;
    }
    
    //Realiza la proyección en memoria
  if ((void *)(datosb=(DatosMemCompartida *)(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE,
			MAP_SHARED, fd, 0))) == (void *)MAP_FAILED) {
		perror("Error en la proyeccion del archivo");
		close(fd);
		return 1;
	}
	
	

	close(fd);
	
	while(datosb->cerrar_mundo == 0){
				
		if (datosb->esfera.centro.y > 1+datosb->raqueta1.y2+(((datosb->raqueta1.y1 - datosb->raqueta1.y2)/2))){
			datosb->accion = 1;
		}
		if (datosb->esfera.centro.y < -1+datosb->raqueta1.y2+(((datosb->raqueta1.y1 - datosb->raqueta1.y2)/2))){
			datosb->accion = -1;
		}
		/*if (datosb->esfera.centro.y == datosb->raqueta1.y2+(((datosb->raqueta1.y1 - datosb->raqueta1.y2)/2))){
			datosb->accion = 0;
		}*/
		if (datosb->esfera.centro.y < 1+datosb->raqueta1.y2+(((datosb->raqueta1.y1 - datosb->raqueta1.y2)/2))&&(datosb->esfera.centro.y > -1+datosb->raqueta1.y2+(((datosb->raqueta1.y1 - datosb->raqueta1.y2)/2)))){
			datosb->accion = 0;
		}
				
		usleep(25000);
	}
    
    munmap(datosb, sizeof(DatosMemCompartida));
    return 0;
}

    
   
